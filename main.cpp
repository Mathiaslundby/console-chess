#include <iostream>
#include "square.h"

using namespace std;


//Draws a character representing the piece
void drawPiece(Piece piece) {
	char p;
	switch (piece.type)
	{
	case Piece::PAWN:
		p = 'P';
		break;
	
	case Piece::ROOK:
		p = 'R';
		break;

	case Piece::KNIGHT:
		p = 'N';
		break;

	case Piece::BISHOP:
		p = 'B';
		break;

	case Piece::QUEEN:
		p = 'Q';
		break;

	case Piece::KING:
		p = 'K';
		break;

	default:
		p = 'A';
		break;
	}

	//If the piece is black, make it lowercase to differentiate
	if (piece.color == Piece::BLACK) {
		p = tolower(p);
	}
	cout << p;
}


//Function for drawing the board in the console
void drawBoard(Square board[8][8]) {
	//Clear console before drawing board
	system("CLS");

	//Define square colors
	char whiteSpace = char(0xDB);
	char blackSpace = char(0xFF);
	char color = whiteSpace;

	//Declaring integers to keep track of which color to draw and which row we are on
	int colorCounter = 0;
	int rowCounter = 8;

	cout << "+----------------------------------------+\n";
	
	//8 iterations because there are 8 squares in a column
	for (int l = 7; l >= 0; l--) {

		//k < 3 because each square takes three lines
		for (int k = 0; k < 3; k++) {

			//If we are on the middle of the line, start by saying which row we re on
			if (k == 1) {
				cout << rowCounter;
				rowCounter--;
			}
			else {
				cout << "|";
			}

			//i < 8 because there are 8 squares on a row
			for (int i = 0; i < 8; i++) {
				Square currentSquare = board[l][i];

				//Define what color the square is going to be
				if (colorCounter % 2 == 0) {
					color = whiteSpace;
				}
				else {
					color = blackSpace;
				}

				//If we are in the middle of a square, find out if there is a piece on the square
				//Split the for loops to reduce operations, makes for worse readability. Worth?
				if (k == 1) {

					//j < 5 because there are 5 characters in a squares width
					for (int j = 0; j < 5; j++) {
						
						//When the middle of the square is reached, look for a piece and draw if it's there
						if (j == 2) {
							
							//If there is a piece on the square, draw it.
							if (currentSquare.piece.type != Piece::NONE) {
								drawPiece(currentSquare.piece);
							}
							else {
								cout << color;
							}
						}
						else {
							cout << color;
						}
					}
				}

				//When not on a middle row, just draw colors
				else {

					//j < 5 because there are 5 characters in a squares width
					for (int j = 0; j < 5; j++) {
						cout << color;
					}
				}

				//Increase colorcounter everytime a square is drawn to change the color of the next square
				colorCounter++;
			}

			//Draw the edge of the board
			cout << "|\n";
		}

		//when we switch row we need to make sure the next square is the same color as the last one drawn
		colorCounter++;
	}
	
	//Draw the bottom edge of the board
	cout << "+--A----B----C----D----E----F----G----H--+\n";
}


void generateSquares(Square board[8][8]) {
	//colorCounter is used to define what color a square is
	int colorCounter = 0;

	//Make 8 rows of squares. Start on 8 beacuse the first row will be labeled 8.
	for (int i = 8; i > 0; i--) {

		//Columns are found using characters. a will be the first column
		char col = 'a';

		//Make 8 squares on the current row, changing color every time
		for (int j = 0; j < 8; j++) {

			Square square(i, col, colorCounter);
			board[i - 1][j] = square;
			colorCounter++;
			col++;
		}

		//when we switch row we need to make sure the next square is the same color as the last one
		colorCounter++;
	}
}


//Initially generates the pieces on the board
void generatePieces(Square board[8][8]) {

	//Generate white pawns on the second row
	for (int i = 0; i < 8; i++) {
		Piece pawn(Piece::PAWN, Piece::WHITE);
		board[1][i].piece = pawn;
	}

	//Generate black pawns on the seventh row
	for (int i = 0; i < 8; i++) {
		Piece pawn(Piece::PAWN, Piece::BLACK);
		board[6][i].piece = pawn;
	}

	//Generate rooks
	Piece rook(Piece::ROOK, Piece::WHITE);
	board[0][0].piece = rook;
	board[0][7].piece = rook;
	rook.color = Piece::BLACK;
	board[7][0].piece = rook;
	board[7][7].piece = rook;

	//Generate knights
	Piece knight(Piece::KNIGHT, Piece::WHITE);
	board[0][1].piece = knight;
	board[0][6].piece = knight;
	knight.color = Piece::BLACK;
	board[7][1].piece = knight;
	board[7][6].piece = knight;

	//Generate bishops
	Piece bishop(Piece::BISHOP, Piece::WHITE);
	board[0][2].piece = bishop;
	board[0][5].piece = bishop;
	bishop.color = Piece::BLACK;
	board[7][2].piece = bishop;
	board[7][5].piece = bishop;

	//Generate queens
	Piece queen(Piece::QUEEN, Piece::WHITE);
	board[0][3].piece = queen;
	queen.color = Piece::BLACK;
	board[7][3].piece = queen;

	//Generate kings
	Piece king(Piece::KING, Piece::WHITE);
	board[0][4].piece = king;
	king.color = Piece::BLACK;
	board[7][4].piece = king;


}


//Function that returns pointer to a square, found using the column and row given as input
//If nullptr is returned, the input is invalid
Square* findSquare(string inputSquare, Square board[8][8]) {

	//If the input is not 2 characters, the square is invalid
	if (inputSquare.length() != 2) {

		//Some functions only require one character as input
		if (inputSquare.length() == 1) {
			
			//If input is 'q', exit the program
			if (tolower(inputSquare[0]) == 'q') {
				exit(1);
			}
		}
		return nullptr;
	}

	//Convert the string input to a column between A-H and row between 1-8
	char col;
	int row;
	try {
		col = tolower(inputSquare[0]);
		row = inputSquare[1] - '0';
		if (int(col) < 97 || int(col) > 104) {
			throw int(col);
		}
		if (row < 1 || row > 8) {
			throw row;
		}
	}
	catch (int num) {
		return nullptr;
	}

	//Find the square using the column and row, return the square when found
	int i = 0;
	int j = 0;
	for (; i < 8; i++) {
		if (board[i][j].row != row) {
			continue;
		}
		for (; j < 8; j++) {
			if (board[i][j].column == col) {
				return &board[i][j];
			}
		}
	}
	
	//This is not supposed to be reached
	return nullptr;

}

//Function to make a move. This will be the games' main loop 
void makeMove(Square board[8][8]) {
	while (true) {

		//Select square with the piece to move
		cout << "Select piece to move: ";
		string inputSquare;
		cin >> inputSquare;
		
		Square* selectedSquare = findSquare(inputSquare, board);

		//If nullptr is returned from findSquare, restart loop
		if (!selectedSquare) {
			cout << "Invalid square\n";
			continue;
		}

		//If selected sqaure has no piece, restart loop
		if (selectedSquare->piece.type == Piece::NONE) {
			cout << "No piece found on the square\n";
			continue;
		}

		while (true) {
			//Select target square to move piece to
			cout << "Select square to move to: ";
			cin >> inputSquare;

			Square* targetSquare = findSquare(inputSquare, board);

			if (!targetSquare) {
				cout << "Invalid square\n";
				continue;
			}


			//Make the move and break the loop
			targetSquare->piece = selectedSquare->piece;
			selectedSquare->piece = Piece();
			break;
		}
		
		//Draw the board with the updated piece placements
		drawBoard(board);
	}
}


int main() {

	//Define a 8x8 set of squares that makes the board
	Square board[8][8];
	generateSquares(board);
	generatePieces(board);
	drawBoard(board);
	
	makeMove(board);

	/*
	* Some code to print the actual squares value
	* 
	for (int i = 7; i >= 0; i--) {
		for (int j = 0; j < 8; j++) {
			cout << board[i][j].column;
			cout << board[i][j].row;
			cout << " ";
			if (board[i][j].color == Square::BLACK) {
				cout << "Black";
			}
			else if (board[i][j].color == Square::WHITE){
				cout << "White";
			}
			cout << " - ";
		}
		cout << "\n";
	}
	*/
	

	

	return 0;
}
