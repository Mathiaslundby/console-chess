#include "piece.h"

Piece::Piece(Type TYPE, Color COLOR) {
	type = TYPE;
	color = COLOR;
	hasMoved = false;
}

Piece::Piece() {
	type = NONE;
}