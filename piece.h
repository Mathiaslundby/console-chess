#pragma once
class Piece {
	public:
		enum Type {
			PAWN,
			ROOK,
			KNIGHT,
			BISHOP,
			QUEEN,
			KING,
			NONE
		};
		enum Color {
			WHITE,
			BLACK
		};

		bool hasMoved;
		Type type;
		Color color;

		Piece(Type TYPE, Color COLOR);
		Piece();
};

