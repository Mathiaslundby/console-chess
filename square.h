#include "piece.h"

class Square {
	public:
		enum Color {
			WHITE,
			BLACK
		};
		Piece piece;
		int row;
		char column;
		Color color;

		Square(int ROW, char COL, int COLOR);
		Square();
};

